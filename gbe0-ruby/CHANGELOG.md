# Change Log

Changelog for the Ruby extension pack.

## [0.0.2] - 2023-12-16

- Switch to Shopify Ruby LSP

## [0.0.1] - 2023-03-25

- Initial release
