# Change Log

Changelog for the base extension pack.

## [0.0.9] - 2023-08-02

- Swap deprecated TOML extension

## [0.0.8] - 2023-07-18

- Remove the GitLab extension - now moved to gbe0.gbe0-gitlab extension pack

## [0.0.7] - 2023-07-18

- Remove the GitHub copilot extension - now moved to gbe0.gbe0-github extension pack

## [0.0.6] - 2023-03-23

- Add extensions

## [0.0.5] - 2023-03-23

- Add extensions

## [0.0.4] - 2023-03-23

- Add extensions

## [0.0.3] - 2023-03-23

- Re-organise extensions

## [0.0.2] - 2023-03-23

- Add extensions

## [0.0.1] - 2023-03-22

- Initial release
