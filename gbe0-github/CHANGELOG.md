# Change Log

Changelog for the GitHub extension pack.

## [0.0.2] - 2023-12-16

- Initial release

## [0.0.1] - 2023-07-18

- Initial release
