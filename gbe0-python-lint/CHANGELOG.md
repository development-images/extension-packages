# Change Log

Changelog for the Python Linting extension pack.

## [0.0.3] - 2023-08-13

- Switch matangover.mypy extension to ms-python.mypy-type-checker

## [0.0.2] - 2023-04-10

- Add Ruff extension

## [0.0.1] - 2023-03-24

- Initial release
